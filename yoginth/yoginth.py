import sys
import superb
from colorama import init, Fore
init()

def main():
	print(Fore.BLUE + '\nHello, world! from Yoginth 🤓\n')
	print(Fore.YELLOW + 'Open-Source Enthusiast\nHardcore Python Geek 🤓\nIssue Resolver\nJack 🃏 of all trades, master of none\nThoughts are my own\nLoves Spotify 🎧 and Code\n')
	print(Fore.BLUE + 'Website -> https://yoginth.ml')
	print(Fore.BLUE + 'Blog -> https://yoginth.ml/blog')
	print(Fore.BLUE + 'AMA -> https://yoginth.ml/ama')
	print(Fore.RED + '\nSocial\n')
	print(Fore.BLUE + 'GitLab -> https://gitlab.com/yoginth')
	print(Fore.BLUE + 'Twitter -> https://twitter.com/yoginths')
	print(Fore.BLUE + 'Facebook -> https://facebook.com/yoginth')
